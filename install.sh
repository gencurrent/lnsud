#!/bin/bash

source settings.conf 
variables=('<project>' '<project_path>' '<project_settings_path>')
# echo "${files[@]}"

files=('uwsgi.ini' 'nginx.conf' 'uwsgi_params')
for file in "${files[@]}"
do
	cat $file > $file'.tmp'
	for var in "${variables[@]}"
	do 
		# echo "<$var>"
		new_value=$(echo $var | sed -e "s/<\(.*\)>/\1/")
		# echo $new_value
		sed -i "s@$var@${!new_value}@" $file.tmp 
	done
done

sudo rm /etc/nginx/sites-enabled/$project
sudo ln -s $project_settings_path/nginx.conf /etc/nginx/sites-enabled/$project
sudo service nginx restart

cd ..
for file in "${files[@]}"
do
    mv lnsud/$file.tmp ./$file
done


